import os

# 指定包含txt文件的文件夹路径
folder_path = "UAV/labels/val"  # 替换为你的文件夹路径

# 获取文件夹内所有txt文件的文件名
txt_files = [f for f in os.listdir(folder_path) if f.endswith(".txt")]

# 用户选择要丢弃的位数和新的字符串排列顺序
discard_indices = [4, 6, 7]  # 选择要丢弃的位数（从0开始计数）
new_order = [5, 0, 1, 2, 3]  # 新的字符串排列顺序

# 处理每个txt文件
for txt_file in txt_files:
    file_path = os.path.join(folder_path, txt_file)
    
    with open(file_path, "r") as file:
        lines = file.readlines()
    
    new_lines = []
    for line in lines:
        data = line.strip().split(",")
        
        for index in discard_indices:
            data[index] = ""
        
        rearranged_data = [data[i] for i in new_order]
        
        new_line = ",".join(rearranged_data) + "\n"
        new_lines.append(new_line)
    
    # 写回到文件
    with open(file_path, "w") as file:
        file.writelines(new_lines)

print("处理完成。")

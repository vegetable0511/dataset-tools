# 目标检测数据集处理工具
* 根据代码注释修改文件夹即可开始转换
* json2txt.py json格式标注转txt
* kmeans.py 使用kmeans算法计算anchors
* normalize.py 标签归一化
* sort.py txt标签顺序调整
* voc2coco.py voc格式标签转coco格式